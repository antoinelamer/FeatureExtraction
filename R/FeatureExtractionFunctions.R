# @file FeatureExtractionFunctions.R
#
#
#
#



#' Minimum value calculation
#'
#' @description
#' \code{minWithoutNoise} calculates the minimum of measurement values excluding artifacts.
#'
#' @usage
#' minWithoutNoise(data, minValue)
#'
#' @param data              DataFrame from \code{importDatabase} function.
#' @param arg               - minValue : Numeric value of the artifacts, 0 by default.
#'

minWithoutNoise = function (data, arg) {

  name = "minWithoutNoise"
  minValue = arg[[1]]

  data = dplyr::group_by(data, periodId, personId)
  dataFilter = dplyr::filter(data, data$valueAsNumber > minValue)
  dataResult = dplyr::summarise(dataFilter, "result"= min(valueAsNumber))

  if (nrow(dataResult) != 0) {
    dataResult = cbind("result" = dataResult$result, name, "startDate" = data[1,3], "endDate" = data[nrow(data),3])
    colnames(dataResult) <- c("value_as_number", "method_concept_id", "startDate", "endDate")
    return(dataResult)
  }
}

#' Maximum value calculation
#'
#' @description
#' \code{maxWithoutNoise} calculates the maximum of measurement values excluding artifacts.
#'
#' @usage
#' maxWithoutNoise(data, maxValue)
#'
#' @param data              DataFrame from \code{importDatabase} function.
#' @param arg               - maxValue : Numeric value of the artifacts.
#'

maxWithoutNoise = function (data, arg) {

  name = "maxWithoutNoise"
  maxValue = arg[[1]]

  data = dplyr::group_by(data, periodId, personId)
  dataFilter = dplyr::filter(data, data$valueAsNumber < maxValue)
  dataResult = dplyr::summarise(dataFilter, "result"= max(valueAsNumber))

  if (nrow(dataResult) != 0){
    dataResult = cbind("result" = dataResult$result, name, "startDate" = data[1,3], "endDate" = data[nrow(data),3])
    colnames(dataResult) <- c("value_as_number","method_concept_id", "startDate", "endDate")
    return(dataResult)
  }
}


#' Mean value calculation
#'
#' @description
#' \code{meanValue} calculates the mean of measurement values excluding artifacts.
#'
#' @usage
#' meanValue(data, min, max)
#'
#' @param data              DataFrame from \code{importDatabase} function.
#' @param arg               - min : Minimum value to be taken into account in the calculation.
#'                          - max : Maximum value to be taken into account in the calculation.
#'

meanValue = function (data, arg) {

  name = "meanValue"
  min = arg[[1]]
  max = arg[[2]]

  data = dplyr::group_by(data, periodId, personId)
  dataFilterMax = dplyr::filter(data, data$valueAsNumber < max)
  dataFilterMin = dplyr::filter(dataFilterMax, dataResult$valueAsNumber > min)
  result = dplyr::summarise(dataFilterMin, "result"= mean(valueAsNumber))

  if (nrow(result) != 0) {
    dataResult = cbind(result$result, name, data[1,3], data[nrow(data),3])
    colnames(dataResult) <- c("value_as_number","method_concept_id", "startDate", "endDate")
    return(dataResult)
  }
}


#' Area Under the Curve
#'
#' @description
#' \code{areaBetweenCurveThreshold} calculates the area under
#' the curve for datetime according to the values of each of the measurements.
#'
#' @usage
#' areaBetweenCurveThreshold(data)
#'
#' @param data              DataFrame from \code{importDatabase} function.
#' @param arg               - threshold : A numeric variable of the threshold chosen. Limit at which episode detection is done
#'                          - operator : A character variable of the operator chosen (<, >, ...)
#'                          which indicates the direction of calculation of the time periods in relation to the threshold
#'

areaBetweenCurveThreshold = function (data, arg) {

  threshold = arg[[1]]
  operator = arg[[2]]

  name = "areaBetweenCurveThreshold"
  integraleMeasurement = 0
  integraleThreshold = 0

  if (nrow(data) > 1) {
    for (i in 1:(nrow(data)-1)){

      if (operator == "<") {
        if (data[i,4] < threshold) {
          integraleThreshold = integraleThreshold + threshold*(data[i+1,3]-data[i,3])
          integraleMeasurement = integraleMeasurement + data[i,4]*(data[i+1,3]-data[i,3])
          integrale = integraleThreshold - integraleMeasurement
        }
      }
      if (operator == ">") {
        if (data[i,4] > threshold) {
          integraleThreshold = integraleThreshold + threshold*(data[i+1,3]-data[i,3])
          integraleMeasurement = integraleMeasurement + data[i,4]*(data[i+1,3]-data[i,3])
          integrale = integraleMeasurement - integraleThreshold
        }
      }
    }

    if (nrow(integrale) != 0) {
      dataResult = cbind(integrale, name, data[1,3], data[nrow(data),3])
      colnames(dataResult) <- c("value_as_number","method_concept_id", "startDate", "endDate")
      dataResult$value_as_number = as.numeric(dataResult$value_as_number)
      return(dataResult)
    }
  }
}
