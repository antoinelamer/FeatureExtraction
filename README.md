# FeatureExtraction
(...contributions)

# Introduction
FeatureExtraction is a R package for the feature extraction from time series data (e.g. heart rate signal, psychiatric assessement scales or biology results).

A **feature** is characterized by :
- a statistical unit (e.g. a person or a visit, depending on the study)
- a signal parameter (e.g. heart rate)
- a period of interest (e.g. Visit in intensive care unit)
- an extraction method (e.g. mean value)

This package offers several extraction methods such as : 
- all usual R methods (min, max, mean, etc.)
- average value, minimum value, maximum value without artefacts (in excluding values outside threshold values)
- automatic detection of time periods outside a predefined threshold (based on the algorithm defined [here](https://pubmed.ncbi.nlm.nih.gov/26817405/))
- user-defined functions

<img src="https://gitlab.com/antoinelamer/FeatureExtraction/-/raw/master/extras/hypotension.png" alt="MAP and hypotension" width="500"/>

The results of these functions are stored in the Feature table of a SQL database (script [here](https://gitlab.com/antoinelamer/FeatureExtraction/-/raw/master/feature.sql)).

# Functionalities
- Extract the necessary data from a database in OMOP Common Data Model format (according to predefined period of interest)
- Calculate features based on a R usual function or a user-defined function
- Store results in OMOP format
- Pivot features from long to wide to facilitate data analysis

# Case studies
Different case studies are available and illustrated in the vignette. 
SQL and R scripts are available to create the period tables and execute the feature extraction for each of the case studies.

**Case study 1:** Detection of acute renal failure during the hospital stay.<br />
**Case study 2:** Hypotension during the first post-operative ICU stay.<br />
**Case study 3:** Psychiatric scale after a visit (or new treatment).<br />
**Case study 4:** Duration of hypotension during general anesthesia.<br />
**Case study 5:** Measures between the administration of a drug and a procedure.

# Results
![](extras/resultHypotension.png)

# Installation 

1. Installing RStudio 
2. In R this command installs the FeatureExtraction package:

```
install.packages("drat")
drat::addRepo("antoinelamer/FeatureExtraction")
install.packages("FeatureExtraction")
```

To have access to the documentation and details of the different functions of the package just run the command:

```
?FeatureExtraction
```

# Documentation

PDF versions of the documentation are also available:
- Vignette: [Integration and aggregation of raw data into the OMOP CDM](https://gitlab.com/antoinelamer/FeatureExtraction/vignettes/FeatureExtraction.pdf)

# TODO

- [ ] interpolation method : Last Observation Carrier Forward, linear, or cubic spline.
- [ ] Method for period detection
- [ ] Spark implementation