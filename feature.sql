
DROP TABLE if exists feature.period;

CREATE TABLE if not exists feature.period (
  period_id							INTEGER			NOT NULL ,
  person_id							INTEGER			NOT NULL ,
  period_concept_id					INTEGER			NOT NULL ,
  start_period_event_id				INTEGER			NULL ,
  start_event_field_concept_id		INTEGER			NULL ,
  period_start_date					DATE			NULL ,
  period_start_datetime				TIMESTAMP		NULL ,
  end_period_event_id				INTEGER			NULL ,
  end_event_field_concept_id		INTEGER			NULL ,
  period_end_date					DATE			NULL ,
  period_end_datetime				TIMESTAMP		NULL ,	
  duration							INTEGER			NULL ,
  duration_unit_concept_id			INTEGER			NULL ,
  visit_occurrence_id				INTEGER			NULL ,
  visit_detail_id					INTEGER         NULL ,
  period_type_concept_id			INTEGER			NULL
);


DROP TABLE if exists feature.feature;

CREATE TABLE feature.feature (
  feature_id					INTEGER			NOT NULL ,
  person_id								INTEGER			NOT NULL ,
  period_id								INTEGER			NOT NULL ,
  feature_concept_id					INTEGER	NOT NULL,
  period_concept_id						INTEGER			NULL ,
  raw_data_concept_id					INTEGER			NULL ,
  feature_method_concept_id				INTEGER			NULL ,
  value_as_number						NUMERIC			NULL ,
  value_as_concept_id					INTEGER			NULL ,
  unit_concept_id						INTEGER			NULL ,
  visit_occurrence_id					INTEGER			NULL ,
  visit_detail_id						INTEGER         NULL ,
  unit_source_concept_id				INTEGER			NULL ,
  feature_type_concept_id				INTEGER			NULL 
);

-- Primary Key

ALTER TABLE feature.period ADD CONSTRAINT xpk_period PRIMARY KEY ( period_id ) ;

ALTER TABLE feature.feature ADD CONSTRAINT xpk_period_aggregate PRIMARY KEY (feature_id);

-- Index

CREATE INDEX idx_period_id  ON feature.period  (period_id ASC);

CREATE INDEX idx_feature_id ON feature.feature (feature_id ASC);

-- Foreign Key

ALTER TABLE period ADD CONSTRAINT fpk_period_person FOREIGN KEY (person_id)  REFERENCES person (person_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_concept FOREIGN KEY (period_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_start_event FOREIGN KEY (start_event_field_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_end_event FOREIGN KEY (end_event_field_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_duration_unit FOREIGN KEY (duration_unit_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_visit_occurrence FOREIGN KEY (visit_occurrence_id)  REFERENCES visit_occurrence (visit_occurrence_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_visit_detail FOREIGN KEY (visit_detail_id)  REFERENCES visit_detail (visit_detail_id);

ALTER TABLE period ADD CONSTRAINT fpk_period_type_concept FOREIGN KEY (period_type_concept_id)  REFERENCES concept (concept_id);


ALTER TABLE period ADD CONSTRAINT fpk_aggregate_person FOREIGN KEY (person_id)  REFERENCES person (person_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_period FOREIGN KEY (period_id)  REFERENCES period (period_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_period_concept FOREIGN KEY (period_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_period_concept FOREIGN KEY (period_concept_id)  REFERENCES concept (concept_id); 

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_raw_data_concept FOREIGN KEY (raw_data_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_method_concept FOREIGN KEY (aggregation_method_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_value_concept FOREIGN KEY (value_as_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_duration_concept FOREIGN KEY (duration_unit_concept_id)  REFERENCES concept (concept_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_visit_occurrence FOREIGN KEY (visit_occurrence_id)  REFERENCES visit_occurrence (visit_occurrence_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_visit_detail FOREIGN KEY (visit_detail_id)  REFERENCES visit_detail (visit_detail_id);

ALTER TABLE period ADD CONSTRAINT fpk_aggregate_type_concept FOREIGN KEY (aggregate_type_concept_id)  REFERENCES concept (concept_id);